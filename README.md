Dataservice automation tests POC
================================
    
    ./gradlew clean test



Spock Framework Example Project
===============================

The purpose of this project is to help you get started with Spock. The project includes several example specifications.

Further Resources
-----------------

* [Spock homepage](http://spockframework.org)
* [Spock web console](https://meetspock.appspot.com)
* [Main documentation](http://wiki.spockframework.org/SpockBasics)
* [User discussion group](http://forum.spockframework.org)
* [Dev discussion group](http://dev.forum.spockframework.org)
* [Issue tracker](http://issues.spockframework.org)
* [Build server](http://builds.spockframework.org)
* [Maven repository](http://m2repo.spockframework.org) - releases are also available from Maven Central
* [Spock blog](http://blog.spockframework.org)
* [Gradle homepage](http://www.gradle.org)
* [Groovy homepage](http://groovy.codehaus.org)

If you have any comments or questions, please direct them to the Spock discussion group. All feedback is appreciated!

Happy spec'ing!
Peter Niederwieser
Creator, Spock Framework

