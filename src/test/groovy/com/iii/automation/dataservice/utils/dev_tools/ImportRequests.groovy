package com.iii.automation.dataservice.utils.dev_tools

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.SwingUtilities

class ImportRequests extends JPanel implements ActionListener {
    JButton openButton, saveButton;
    JTextArea out;
    JFileChooser fc;


    private String parsePostmanFile(String path){
        try {
            def file = new File(path)
            def parser = new JsonSlurper()
            def jsonFile = parser.parseText(file.getText())

            Set<Map> reqs = new HashSet<>()
            jsonFile.item.each {
                Map m = [
                        "path"             : it.request.url,
                        "response_filename": it.name,
                        "method"           : it.request.method,
                        "body"             : it.request.body.raw,
                        "headers"          : [:]
                ]
                it.request.header.each {
                    m.get("headers").put(it.key, it.value)
                }

                reqs.add(m)
            }
            JsonOutput.toJson(reqs).with { JsonOutput.prettyPrint(it) }
        } catch (all){
            all.printStackTrace()
            "Can't parse $path:\n$all"
        }
    }

    ImportRequests() {
        super(new BorderLayout());

        out = new JTextArea(20,80);
        out.setMargin(new Insets(10,10,0,0));
        out.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(out);

        fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

        openButton = new JButton("Open file");
        openButton.addActionListener(this);

        saveButton = new JButton("Copy Output to Clipboard");
        saveButton.addActionListener(this);

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(openButton);
        buttonPanel.add(saveButton);

        add(buttonPanel, BorderLayout.PAGE_START);
        add(logScrollPane, BorderLayout.CENTER);
    }

    void actionPerformed(ActionEvent e) {

        if (e.getSource() == openButton) {
            int returnVal = fc.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                out.setText(parsePostmanFile(file.path))        /* Postman output */
            }
            out.setCaretPosition(out.getDocument().getLength());

        } else if (e.getSource() == saveButton) {               /* Copy to clipboard */
            StringSelection stringSelection = new StringSelection(out.getText());
            Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
            clip.setContents(stringSelection, null);
        }
    }


    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Helper Tool for creating new requests file");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new ImportRequests());
        frame.pack();
        frame.setVisible(true);
    }

    static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

