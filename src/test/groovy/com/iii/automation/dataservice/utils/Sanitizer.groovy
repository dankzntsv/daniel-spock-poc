package com.iii.automation.dataservice.utils

import com.iii.automation.dataservice.ego.EgoSanitizer

class Sanitizer {

    static time(it) {
        it.replaceAll(/(Time:).\d{4,}/, "Time:scrubbed")
    }

    static firstValue(it){
        it.replaceFirst(/"value":.*"/, '"value": "scrubbed"')
    }

    static revision(it) {
        it.replaceAll(/("Revision:).*",/, '"Revision:scrubbed",')
    }

}

