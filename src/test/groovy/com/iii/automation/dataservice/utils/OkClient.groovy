package com.iii.automation.dataservice.utils

import groovy.json.JsonSlurper
import groovy.util.logging.Log4j
import okhttp3.OkHttpClient;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Call;
import okhttp3.Callback
import org.spockframework.builder.SpockConfigurationGestalt
import org.spockframework.runtime.SpockRuntime

import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

/**
 * This class is a horrible mix of Joovy
 */

@Log4j
class OkClient {

    static Properties CFG
    private final OkHttpClient client = new OkHttpClient().newBuilder()
                                        .cache(null)
                                        .readTimeout(30, TimeUnit.SECONDS)
                                        .connectTimeout(10, TimeUnit.SECONDS)
                                        .build()
    public static final MediaType RAW = MediaType.parse("");
    private AtomicInteger requestsLeft

    //TODO move thes to config
    //final String CONTEXT = "/sierra/ws/sierra"
    //final String BASE_URL = "http://10.1.125.174:63000"
    //final String BASE_URL = "https://archdev.iii.com:63100"
    //final String BASE_URL = "http://dev-api.iii.com:63000"
    //final String BASE_URL = "http://devops-4759-app.iii-lab.us:63000"


    /**
     *   Send asynchronous requests
     */
    def playRequests(FileUtil fUtil, Map responseMap = null){
        def reqFile = new File(getClass().getResource("/$fUtil.endpoint/requests.json").toURI())
        def parser = new JsonSlurper()
        def reqJson = parser.parseText(reqFile.getText())
        requestsLeft = new AtomicInteger(reqJson.size())
        reqJson.each {
            def callback
            if(responseMap==null)
                callback = new RecordAndClose( fileUtil: fUtil,
                                               fileName: it.response_filename)
            else
                callback = new ReturnAndClose( fileUtil: fUtil,
                                               fileName: it.response_filename,
                                               responseMap: responseMap)

            sendRequest(it, callback)
        }

        while(!client.dispatcher().executorService().terminated)
            sleep(500)
        log.info("All requests completed for $fUtil.endpoint")
        responseMap ?: null
    }

    private void sendRequest(reqJson, callback){
        def req = new Request.Builder().url(CFG.BASE_URL + CFG.CONTEXT + reqJson.path)

        switch (reqJson.method.toLowerCase()){
            case 'post':
                req.post(RequestBody.create(RAW, PwGen.attachAuth(reqJson.body)))
                break
            case 'get':
                req.get()
                break
            default:
                throw new IllegalArgumentException("Unable to send request for $reqJson.response_filename. Unrecognized method ($reqJson.method)")
        }

        reqJson.headers.each {
            req.addHeader(it.key, it.value)}

        log.debug("sent: $reqJson.response_filename")
        client.newCall(req.build()).enqueue(callback)
    }


    private class RecordAndClose implements Callback {
        FileUtil fileUtil
        def fileName

        private void isLastRequest(){
            if(requestsLeft.decrementAndGet()==0){
                log.debug("Last request completed for $fileUtil.endpoint")
                client.dispatcher().executorService().shutdown();
            }
        }

        @Override
        public void onFailure(Call call, IOException e) {
            log.error("$fileName: failed reqest: ${e.getMessage()}", e)
            isLastRequest()
        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            long time = response.receivedResponseAtMillis()-response.sentRequestAtMillis()
            //log.debug("${time/1000.0}s response: $fileName")
            fileUtil.recordResponse(response, fileName)
            response.close()
            isLastRequest()
        }
    }

    private class ReturnAndClose extends RecordAndClose {
        Map responseMap

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            long time = response.receivedResponseAtMillis()-response.sentRequestAtMillis()
            log.debug("${time/1000.0}s response: $fileName")
            responseMap.put(fileName, response)
            response.close()
            super.isLastRequest()
        }
    }
}