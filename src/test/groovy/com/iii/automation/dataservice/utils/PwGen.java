package com.iii.automation.dataservice.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class PwGen {

    private static final int[] map = new int[]{9, 5, 2, 3, 8, 6, 7, 8, 1, 4};
    private static final String baseDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    private static String convert(long nOrig) {
        int mod = 0;
        long nValue = nOrig;
        String sValue = "";
        while (nValue != 0) {
            mod = (int) (nValue % 62);
            sValue = baseDigits.substring(mod, mod + 1) + sValue;
            nValue = nValue / 62;
        }
        nValue = nOrig >> 1;
        while (nValue != 0) {
            mod = (int) (nValue % 62);
            sValue = baseDigits.substring(mod, mod + 1) + sValue;
            nValue = nValue / 62;
        }
        return sValue;
    }

    private static String get(String key) {
        try {
            int nStart = Integer.parseInt(key);
            long nMult = (nStart % 6399) * (nStart % 93827);
            long nValue = nStart + (nMult * map[nStart % 10]);
            return convert(nValue);
        } catch (Exception e) {

            return "\1\2\3\4";
        }
    }

    private static String get(String key, String altKey) {
        try {
            int nStart = Integer.parseInt(key);
            long nMult = (nStart % 6399) * (nStart % 93827);
            long nValue = nStart + (nMult * map[nStart % 10]);
            nValue += stringHash(altKey);
            return convert(nValue);
        } catch (Exception e) {

            return "\1\2\3\4";
        }
    }

    private static int stringHash(String s) {
        int hash = 0;
        for (int i = 0; i < s.length() && i < 3; i++) {
            hash = 31 * hash + s.charAt(i);
        }
        return hash;
    }

    public static String getUserPass(){
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormatter = new SimpleDateFormat("yyyyMMddHH");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        cal.add(Calendar.HOUR_OF_DAY, 1);
        String pass = PwGen.get(dateFormatter.format(cal.getTime()));
        return  "<entry key=\"username\">" +
                    "<value>sierra001</value>" +
                    "</entry>" +
                    "<entry key=\"password\">" +
                    "<value>"+pass+"</value>" +
                    "</entry>";
    }

    public static String attachAuth(String body){
        if(body.contains("<entry key=\"password\">"))
            return body;
        if(body.contains("<body>"))
            return body.replaceFirst("<body>", "<body>"+getUserPass());
        if(body.contains("<body />"))
            return body.replaceFirst("<body />", "<body>"+getUserPass()+"</body>");
        else
            return body;
    }

}
