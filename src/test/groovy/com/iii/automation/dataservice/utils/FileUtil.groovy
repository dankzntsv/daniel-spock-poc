package com.iii.automation.dataservice.utils;

import groovy.io.FileType
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.util.logging.Log4j
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import org.json.XML

import static org.hamcrest.text.IsEqualIgnoringWhiteSpace.equalToIgnoringWhiteSpace

@Log4j
class FileUtil {

    Map filters
    String endpoint

    FileUtil(String endpoint){
        assert !endpoint.isEmpty()
        this.endpoint = endpoint
    }

    boolean baselineEqualTo(String testName){
        if(equalToIgnoringWhiteSpace(
                    getFileContent("/$endpoint/responses/$testName"))
                    .matches(
                    getFileContent("/$endpoint/baselines/$testName"))) {
            true
        } else
            false
    }

//    String baseline(String testName) {
//        getFileContent("/$endpoint/baselines/$testName")
//    }

    def response(String testName) {
        //getFileContent("/$endpoint/responses/$testName")
        def parser = new JsonSlurper()
        parser.parseText(getFileContent("/$endpoint/responses/$testName"))
    }

    private static String getFileContent(String file) {
        try {
            def f = new File(getClass().getResource("${file}.json").toURI())
            f.getText()
        } catch (all) {
            def msg = "Error reading file $file"
            log.error("$msg: ${all.getMessage()}")
            return msg
        }

    }

    Collection<String> getBaselinesList(){
        def list = []
        def dir = new File(getClass().getResource("/$endpoint/baselines/").toURI())
        dir.eachFileRecurse (FileType.FILES) { f ->
            list << f.getName().replace('.json', '')
        }
        list
    }

    String getDiff(String testName){
        FileDiff.getDiff(endpoint, "${testName}.json")
    }

    private void recordResponse(Response response, String fileName){
        def body
        try {
            body = response.body().string()
            long t = System.currentTimeMillis()-response.receivedResponseAtMillis()
            body = xmlToMap(body)

        }catch (SocketTimeoutException e){
            body = "$fileName: ${e}"
            log.error("$fileName: $e")
        }catch (JSONException e){
            log.warn("$fileName: Invalid xml body received, saving as plainntext. ${e.getMessage()}")
        }
        def headers = response.headers().toMultimap()
        def code = response.code();

        def json = JsonOutput.toJson([status_code    : code,
                                      header_response: headers,
                                      body_response  : body ])
                .with {JsonOutput.prettyPrint(it)}
        def file = new File(getClass().getResource("/$endpoint/responses/").getPath()+"${fileName}.log")
        file.write(json)
        log.debug("saved: "+file.getPath())

        recordSanitizedResponse(body, headers, code, fileName)
    }

    private void recordSanitizedResponse(body, headers, code, fileName){
        def json = JsonOutput.toJson([status_code: code,
                                      header_response: ['content-type': headers.'content-type',
                                                        'accept': headers.accept
                                      ],
                                      body_response: body
        ]).with {JsonOutput.prettyPrint(it)}
        def file = new File(getClass().getResource("/$endpoint/responses/").getPath()+"${fileName}.json")

        if(filters?.containsKey(fileName)){
            filters.get(fileName).each {
                json = it(json)
            }
        }

        file.write(json)

        log.debug("sanitized to "+file.getPath())
    }

    private def xmlToMap(String xml) throws JSONException{
        JSONObject xmlJSONObj = XML.toJSONObject(xml)
        xmlJSONObj.toMap()
    }
}
