package com.iii.automation.dataservice.utils

import difflib.Delta
import difflib.DiffUtils
import difflib.Patch
import groovy.util.logging.Log4j

@Log4j
class FileDiff {



    protected static String getDiff(String endpoint, String file) {
        StringBuilder diff = new StringBuilder("")
        List<String> original = fileToLines("/$endpoint/baselines/$file");
        List<String> revised = fileToLines("/$endpoint/responses/$file");

        // Compute diff. Get the Patch object. Patch is the container for computed deltas.
        Patch patch = DiffUtils.diff(original, revised);
        log.debug("Diffing: $endpoint.$file")
        if(!patch.getDeltas().isEmpty()){
            for (Delta d : patch.getDeltas()) {
                diff.append(d.toString().replaceAll("\\s+"," ")
                                        .replaceFirst(", lines: ", "]\n")
                                        .replaceFirst("] to \\[", "]\n< < < vs > > >\n[")
                                        ).append("\n")
            }
        }
        diff
    }
    public static Patch ddd(String endpoint, String file) {
        file = file+".json"
        StringBuilder diff = new StringBuilder("")
        List<String> original = fileToLines("/$endpoint/baselines/$file");
        List<String> revised = fileToLines("/$endpoint/responses/$file");

        // Compute diff. Get the Patch object. Patch is the container for computed deltas.
        Patch patch = DiffUtils.diff(original, revised);
        patch
    }
    private static List fileToLines(String str) {
        List lines = new LinkedList(); String line = "";
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(str)));
            while ((line = br.readLine()) != null) {
                lines.add(line);
                if(lines.size()>1000){
                    lines.add("File too large. Diff tool skipping rest..."+System.identityHashCode(lines))
                    break
                }
            }
        } catch (IOException | NullPointerException e) {
            log.error("Diff failed: $e")
            lines.add(e.getMessage())
        }
        return lines;
    }
}
