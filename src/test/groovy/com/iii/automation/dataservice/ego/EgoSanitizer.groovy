package com.iii.automation.dataservice.ego

import com.iii.automation.dataservice.utils.Sanitizer

class EgoSanitizer extends Sanitizer {


    static unixTime(it) {
        it.replaceAll(/\d{13}/, 'scrubbed')
    }

    static userDefinedCodes(it) {
        it.replaceAll(/PersistentUserDefinedPropertyManager:.*"/, 'PersistentUserDefinedPropertyManager:scrubbed"')
    }
}
