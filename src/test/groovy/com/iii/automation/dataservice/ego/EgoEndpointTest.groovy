package com.iii.automation.dataservice.ego

import com.iii.automation.dataservice.utils.FileDiff
import difflib.*
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import com.iii.automation.dataservice.utils.FileUtil
import com.iii.automation.dataservice.utils.OkClient


@Unroll
class EgoEndpointTest extends Specification {

    @Shared ego = new FileUtil('ego')

    def setupSpec() {

        def scrub = EgoSanitizer.class

        def map = ['getFullVersion': [scrub.&revision],
                   'listEndpoints': [scrub.&time, scrub.&userDefinedCodes],
                   'getTime': [scrub.&unixTime],
                   'getVersion': [scrub.&firstValue],
                   'listMethods': [scrub.&time, scrub.&userDefinedCodes]
        ]

        ego.with {filters = map}
        new OkClient().playRequests(ego)
    }

    def "Ego - #testName"() {

        given:
        def match = ego.baselineEqualTo(testName)

        expect:
        if (match)
            assert match
        else
            assert match : "File diff:\n ${ego.getDiff(testName)}"

        where:
        testName <<  ego.getBaselinesList()
    }




}