package com.iii.automation.dataservice.negative_tests

import com.iii.automation.dataservice.utils.FileUtil
import com.iii.automation.dataservice.utils.OkClient
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class NegativeTest extends Specification {

    @Shared negative = new FileUtil('negative_tests')

    def setupSpec() {

        new OkClient().playRequests(negative)
    }

    def "#testName"() {

        given:
        def match = negative.baselineEqualTo(testName)

        expect:
        if (match)
            assert match
        else
            assert match : "File diff:\n ${negative.getDiff(testName)}"

        where:
        testName <<  negative.getBaselinesList()
    }

    def "Bad content-type header exception"(){
        given:
        def response = negative.response("negative-invalid_content_type")

        expect:
        with(response) {
            verifyAll {
                status_code == 500
                body_response.contains('Invalid Content-Type:text/bad_xml')
                body_response.contains('Is this an error message instead of a SOAP response?')
            }
        }
    }

}