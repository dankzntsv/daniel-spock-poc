package com.iii.automation.dataservice.performance

import groovy.json.internal.LazyMap
import groovy.util.logging.Log4j
import okhttp3.Response
import spock.lang.Shared
import spock.lang.Specification
import com.iii.automation.dataservice.utils.FileUtil
import com.iii.automation.dataservice.utils.OkClient


class ResponseTimeTest  extends Specification {


    @Shared perf = new FileUtil('performance')



    def "Average response time"() {

        given:
        def responses = new OkClient().playRequests(perf, new LazyMap(10))
        def totalTime = 0

        expect:
        verifyAll {
            responses.each {
                Response res = it.getValue()
                long time = res.receivedResponseAtMillis() - res.sentRequestAtMillis()
                double sec = time / 1000.0
                totalTime += sec
                println("Delay(s): ${sec} \tStatus: $res.code \tRequest: $it.key")
                assert sec < 5 : "slow response time..."
            }
            double average = totalTime / responses.size()
            average.round(2) < 1.0
        }
    }


}