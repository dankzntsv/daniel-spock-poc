package com.iii.automation.dataservice.darc

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import com.iii.automation.dataservice.utils.FileUtil
import com.iii.automation.dataservice.utils.OkClient

@Unroll

class DarcEndpointTest extends Specification {

    @Shared darc = new FileUtil('darc')

    def setupSpec() {

        def scrub = DarcSanitizer

        def map = [
                'darc-listEndpoints': [scrub.&time],
                'darc-getFullVersion': [scrub.&revision],
                'darc-getVersion': [scrub.&firstValue],
                'darc-listEndpoints': [scrub.&userDefined, scrub.&time]
        ]

        darc.with {filters = map}
        new OkClient().playRequests(darc)
    }

    def "#testName"() {

        given:
        def match = darc.baselineEqualTo(testName)

        expect:
        if (match)
            assert match
        else
            assert match : "File diff:\n ${darc.getDiff(testName)}"


        where:
        testName <<  darc.getBaselinesList()
    }





}