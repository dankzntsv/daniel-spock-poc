package com.iii.automation.dataservice.darc

import com.iii.automation.dataservice.utils.Sanitizer

class DarcSanitizer extends Sanitizer {

    static userDefined(it) {
        it.replaceAll(/(UserDefined.*):.*"/, "UserDefined:scrubbed\"")
    }


}
