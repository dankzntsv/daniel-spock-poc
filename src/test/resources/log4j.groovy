
log4j {
    appender.CONSOLE = "org.apache.log4j.ConsoleAppender"
    appender."CONSOLE.layout"="org.apache.log4j.PatternLayout"
    appender."CONSOLE.layout.ConversionPattern"="%d{HH:mm:ss,SSS} {%-6p] %c{1}: %m%n"
    appender.FILE = "org.apache.log4j.FileAppender"
    appender."FILE.layout"="org.apache.log4j.PatternLayout"
    appender."FILE.layout.ConversionPattern"="%d [%-6p] %c{1}: %m%n"
    appender."FILE.file"="spock_automation.log"
    rootLogger = "debug,FILE,CONSOLE"
}