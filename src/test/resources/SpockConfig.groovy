import com.iii.automation.dataservice.performance.ResponseTimeTest
import com.iii.automation.dataservice.utils.OkClient
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.log4j.PropertyConfigurator


runner {
    def config = new Properties()
    config.load(getClass().getResource("SpockConfig.properties").openStream())
    OkClient.CFG = config

    def log4j = new ConfigSlurper().parse(new File(getClass().getResource("log4j.groovy").toURI()).toURL())
    PropertyConfigurator.configure(log4j.toProperties())
    Logger log = Logger.getLogger(this.class)
    log.setLevel(Level.toLevel(config.LOG_LEVEL))
    log.info("Loaded config:")
    config.each {
        log.info(it)
    }



    //include Class.forName(endpoint, false, Thread.currentThread().contextClassLoader)
    //exclude ResponseTimeTest

//    println "Optimize run order"
//    optimizeRunOrder true

}

//    report {
//        enabled true
//        logFileDir '.'
//        logFileName 'spock-report.json'
//        logFileSuffix new Date().format('yyyy-MM-dd_HH_mm_ss')
//    }


